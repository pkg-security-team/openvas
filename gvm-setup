#!/bin/bash

## Colour for bash prompt
RED="\033[01;31m"
GREEN="\033[01;32m"
YELLOW="\033[01;33m"
RESET="\033[00m"

## Check if running as root
if [[ $EUID -ne 0 ]]; then
   echo -e "${RED}[-]${RESET} Error: $0 must be run as root" 1>&2
   exit 1
fi

## Check first if we have the correct PostgreSQL version
echo -e "\n${GREEN}[>]${RESET} Starting PostgreSQL service"
systemctl start postgresql
if ! systemctl is-active --quiet postgresql; then
  echo -e "${RED}[-]${RESET} ERROR: PostgreSQL failed to start" 1>&2
  systemctl --no-pager -l status postgresql
  exit 1
fi

postgres_version=$(pg_lsclusters --no-header | awk '$3 == "5432" { print $1 }')

## Get the PostgreSQL version used for gvmd compilation
gvmd_postgres_version=$(cat /usr/share/gvmd/postgresql-version)
gvmd_postgres_version=${gvmd_postgres_version%%.*}

if [ "$postgres_version" = "" ]; then
    echo -e "${RED}[-]${RESET} ERROR: No PostgreSQL version uses the port 5432/TCP" 1>&2
    echo -e "${RED}[-]${RESET} ERROR: libgvmd needs PostgreSQL ${gvmd_postgres_version} to use the port 5432" 1>&2
    echo -e "${RED}[-]${RESET} ERROR: Use pg_upgradecluster to update your PostgreSQL cluster" 1>&2
    exit 1
elif [ ${postgres_version%%.*} -ne $gvmd_postgres_version ]; then
    echo -e "${RED}[-]${RESET} ERROR: The default PostgreSQL version (${postgres_version%%.*}) is not ${gvmd_postgres_version} that is required by libgvmd" 1>&2
    echo -e "${RED}[-]${RESET} ERROR: libgvmd needs PostgreSQL ${gvmd_postgres_version} to use the port 5432" 1>&2
    echo -e "${RED}[-]${RESET} ERROR: Use pg_upgradecluster to update your PostgreSQL cluster" 1>&2
    exit 1
fi

## Generate certs
echo -e "\n${GREEN}[>]${RESET} Creating GVM's certificate files"
runuser -u _gvm -- gvm-manage-certs -a -q -f

## Create database first
echo -e "\n${GREEN}[>]${RESET} Creating PostgreSQL database"
runuser -u postgres -- /usr/share/gvm/create-postgresql-database

## Migrate database if necessary
echo -e "${GREEN}[>]${RESET} Migrating database"
runuser -u _gvm -- gvmd --migrate

## Create Admin User for GVM
echo -e "${GREEN}[>]${RESET} Checking for GVM admin user"
gvmd_users=$(runuser -u _gvm -- gvmd --get-users | grep admin)
if [ -z $gvmd_users ]; then
    echo -e "${GREEN}[*]${RESET} Creating user admin for gvm"
    password=$(runuser -u _gvm -- gvmd --create-user=admin)
    echo -e "${GREEN}[*]${RESET} Please note the generated admin password"
    echo -e "${GREEN}[*]${RESET} $password"
fi

## Configure a Feed Import Owner
echo -e "${GREEN}[*]${RESET} Configure Feed Import Owner"
gvmd_feed_import_owner=$(runuser -u _gvm -- psql -t -c"SELECT value FROM settings WHERE uuid = '78eceaec-3385-11ea-b237-28d24461215b'" gvmd)

if [ -z $gvmd_feed_import_owner ]; then
    echo -e "${GREEN}[*]${RESET} Define Feed Import Owner"
    # get the uuid of the admin user.
    gvmd_uuid_users=$(runuser -u _gvm -- gvmd --get-users --verbose | awk '$1 == "admin" { print $2 }')
    runuser -u _gvm -- gvmd --modify-setting 78eceaec-3385-11ea-b237-28d24461215b --value $gvmd_uuid_users
fi

## Update feeds. Has to be done after the config of a Feed Import Owner
echo -e "${GREEN}[*]${RESET} Update GVM feeds"
greenbone-feed-sync --type all

echo -e "${GREEN}[*]${RESET} Checking Default scanner"
if ! runuser -u _gvm -- gvmd --get-scanners | grep /run/ospd/ospd.sock; then
    echo -e "${GREEN}[*]${RESET} Modifying Default Scanner"
    runuser -u _gvm -- gvmd --modify-scanner=08b69003-5fc2-4037-a479-93b440211c73 --scanner-host=/run/ospd/ospd.sock
else
    echo -e "${YELLOW}[i]${RESET} No need to alter default scanner"
fi

## Done
echo -e "\n${GREEN}[+]${RESET} Done"
if [ -n "$password" ]; then
    echo -e "${GREEN}[*]${RESET} Please note the password for the admin user"
    echo -e "${GREEN}[*]${RESET} $password"
else
    echo -e "${YELLOW}[i]${RESET} Admin user already exists for GVM"
    echo -e "${YELLOW}[i]${RESET} If you have forgotten it, you can change it. See gvmd manpage for more information"
fi

echo -e "\n${GREEN}[>]${RESET} You can now run gvm-check-setup to make sure everything is correctly configured"
